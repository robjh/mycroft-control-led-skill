from mycroft import MycroftSkill, intent_file_handler
import RPi.GPIO as GPIO

class ControlLed(MycroftSkill):
    def __init__(self):
        MycroftSkill.__init__(self)

    def initialize(self):
        self.pinnum = int(self.settings.get('gpio_pin', 25))
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pinnum, GPIO.OUT)

    def shutdown(self):
        0

    @intent_file_handler('led.control.intent')
    def handle_led_control(self, message):
        currently = GPIO.input(self.pinnum)

        action = "toggle"
        if "action" in message.data:
            action = message.data["action"]

        if action == "toggle":
            action = ("on" if currently == 0 else "off")

        if action == "on":
            GPIO.output(self.pinnum, GPIO.HIGH)
            self.speak_dialog('led.on')
            
        if action == "off":
            GPIO.output(self.pinnum, GPIO.LOW)
            self.speak_dialog('led.off')

def create_skill():
    return ControlLed()

