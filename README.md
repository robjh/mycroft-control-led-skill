# <img src="https://raw.githack.com/FortAwesome/Font-Awesome/master/svgs/solid/lightbulb.svg" card_color="#FF0" width="50" height="50" style="vertical-align:bottom"/> Control Led
Controls an external led.

## About
An led is connected to gpio 25, this skill toggles it.

## Examples
* "Turn on the led."
* "Turn the led off."
* "Toggle led."

## Credits
Robin J. Heywood

## Category
**Information**

## Tags

